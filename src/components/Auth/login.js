import Vue from 'vue';
import template from './login.html';

export default Vue.extend({
  template,

  data () {
    return {
      profile: {},
      authorized: false
    }
  },

  computed: {
    msg () {
      if (this.profile.name) {
        return `Welcome <b><i> ${this.profile.name} </i></b>`
      } else {
        return 'Login Facebook to Enjoy the App'
      }
    },

    profilePicture () {
      return (this.profile.id) ? `https://graph.facebook.com/${this.profile.id}/picture?width=50` : ``
    }
  },

  methods: {
    login () {
      let vm = this;

      FB.login(function () {
        vm.getProfile();
      }, {
        scope: 'email, public_profile',
        return_scopes: true
      })
    },

    logout () {
      let vm = this

      FB.logout(function (response) {
        vm.statusChangeCallback(response)
      })
    },

    statusChangeCallback (response) {
      let vm = this;

      if (response.status === 'connected') {
        vm.authorized = true;
        vm.getProfile();
      } else if (response.status === 'not_authorized') {
        vm.authorized = false;
      } else if (response.status === 'unknown') {
        vm.profile = {};
        vm.authorized = false;
      } else {
        vm.authorized = false;
      }
    },

    getProfile () {
      let vm = this;

      FB.api('/me?fields=name,id,email', function (response) {
        vm.$set(vm, 'profile', response)
      })
    },
  },

  mounted () {
    let vm = this
    
    window.fbAsyncInit = function() {
      /*global FB*/

      FB.init({
        appId: '133082387330682',
        cookie: true,
        xfbml: true,
        version: 'v2.10'
      });

      FB.AppEvents.logPageView();

      // Get FB Login Status
      FB.getLoginStatus(response => {
        vm.statusChangeCallback(response)
      })
    };
  }
});
