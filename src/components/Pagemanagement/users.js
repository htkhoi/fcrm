import Vue from 'vue';
import lodash from 'lodash'
import VueLodash from 'vue-lodash'

import template from './users.html';

Vue.use(VueLodash, lodash);

export default {
  template,

  data () {
    return {
      comments: []
    }
  },

  computed: {
    commentsByCreatedTime: function() {
      let vm = this;
      return this._.orderBy(vm.comments, 'created_time', 'desc');
    }
  },

  created() {
    this.fetchComments();
  },

  watch: {
    watch: {
      '$route': 'fetchComments'
    },
  },

  methods: {
    fetchComments() {
      let comments = [];
      
      this.$_FB().then(FB => {
        FB.api('1997670737133192/feed?fields=comments.since(2016-12-23T17:57:5).include_read(true)&{id,created_time,updated_time,unread,object,link}', response => {
          
          if (response.data.length > 0) {
            for (var i = 0; i < response.data.length; i++) {
              if (response.data[i].comments != undefined) {
                for(var j = 0; j < response.data[i].comments.data.length; j++) {
                  response.data[i].comments.data[j].feed = {
                    id: response.data[i].id,
                  };

                  comments.push(response.data[i].comments.data[j]);
                }
              }
            }
          }

          this.comments = comments;

        }, { access_token: this.$_FB_accessToken() })
      })
    }
  }
}
