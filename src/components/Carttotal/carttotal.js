import Vue from 'vue';
import template from './carttotal.html';

export default Vue.extend({
  template,
  computed: {
    itemsInCart() {
      let cart = this.$store.getters.cartProducts;
      return cart.length;
    }
  }
});
