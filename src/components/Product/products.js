import Vue from 'vue';
import template from './products.html';

export default Vue.extend({
  template,

  mounted () {
    console.log(this.$store.state.products);
  }
});
