import Home from 'components/Home/home';
import Posts from 'components/Posts/posts';
import Post from 'components/Posts/post';
import CreatePost from 'components/Posts/createPost';
import EditPost from 'components/Posts/editPost';
import NotFound from 'components/NotFound/notFound';
import Login from 'components/Auth/login';
import ProductList from 'components/Product/products';
import Pagemanagement from 'components/Pagemanagement/users';

const routes = [
  {
    path: '/',
    component: Home
  },
  {
    path: '/posts',
    component: Posts
  },
  {
    path: '/posts/create',
    name: 'createPost',
    component: CreatePost
  },
  {
    path: '/post/:id',
    name: 'post',
    component: Post
  },
  {
    path: '/post/:id/edit',
    name: 'editPost',
    component: EditPost
  },
  {
    path: '/login',
    component: Login
  },
  {
    path: '/products',
    component: ProductList
  },
  {
    path: '/page/users',
    component: Pagemanagement
  },
  {
    path: '*',
    component: NotFound
  }
];

export default routes;
