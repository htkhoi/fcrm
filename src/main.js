import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';

import { LoadingState } from 'src/config/loading-state';
import Navigation from 'components/Navigation/navigation';
import Loader from 'components/Loader/loader';

Vue.use(VueRouter);
Vue.use(Vuex);

import 'src/config/http';
import routes from 'src/routes';
import 'src/style.scss';

const store = new Vuex.Store({
  strict: true,
  state: {
    products: [
      {
        id: 'cc919e21-ae5b-5e1f-d023-c40ee669520c',
        name: 'COBOL 101 vintage',
        description: 'Learn COBOL with this vintage programming book',
        price: 399
      },
    ]
  }
});

Vue.mixin({
  methods: {
    $_FB () {
      if (window.FB) {
        return new Promise(resolve => {
          resolve(window.FB)
        })
      }

      return this.$_FB_init()
    },
    $_FB_appID () {
      return '1594909710529405'
    },
    $_FB_accessToken () {
      return 'EAACEdEose0cBAHoEUtZAufzXRhaLTie41w1qOVBCNOG6AHniD5rrz5abyQDAZCjYRirbOVw6Hhr5gK8P2X85ZAaaiZC62KrJ5C1VcKoPhV7JjioTHMmxXu4Jcl8cpfaFwc5PJt4h3oPYLcnPMpU8v7bxjo4NFIFa9BSIGxOaKvDFUH1ZBogy0yexZCjrZBUWZBoZCLKiWG71vDwZDZD'
    },
    $_FB_init () {
      return new Promise(resolve => {
        window.fbAsyncInit = () => {
          window.FB.init({
            appId: this.$_FB_appID(),
            autoLogAppEvents : true,
            xfbml            : true,
            version          : 'v2.11'
          });

          resolve(window.FB)
        };

        (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "https://connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
      })
    }
  }
})

export const router = new VueRouter({
  routes,
  mode: 'history',
  linkActiveClass: 'active'
});

new Vue({
  router,
  store: store,
  components: {
    Navigation,
    Loader,
  },

  data() {
    return {
      isLoading: false
    };
  },

  created() {
    LoadingState.$on('toggle', (isLoading) => {
      this.isLoading = isLoading;
    });
  }
}).$mount('#app');
